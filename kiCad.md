# Making a circuit Board

To make a PCB board we are going to use [Kicad](https://www.kicad.org/) which is an open source software used for making PCB boards. There are other softwares (eg. Eagle, Altium) you can use but they wont be free like Kicad.
To make a PCb firstly, you need to have the basic knowledge of electronics. Here is a book you can learn from [Encyclopedia of Electronic components](https://drive.google.com/file/d/1AwPrgr1UP9AR9KKMQ9yiUgJsvWK1h-DC/view?usp=sharing).

Lets start with making a new project in Kicad with default library. You will get a new and empty schematic page. 
![](./PcbDesign/sche.png)
 On the right side there are the functionalities given  which can be used by selecting it but we will be using shortcuts;
 a- add components
 w- wire
 m- move the components
 r-rotate


 Adding components:
 ![](./PcbDesign/addingcomponents.png)
 After adding all the components, you have to make the proper connection to your circuits. 
 ![](./PcbDesign/kicad1.png)
 You can change the value of your component also remane them like R1,R2, Led white, etc.
 This [link](https://www.youtube.com/watch?v=-WA7QNinqrk&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=4)has detailed explaination.
  
After making the connection, we have to assign pootprints to all the components.
![](./PcbDesign/assignfootprint.png)
Here you can filter the components and select the components of you choice from the right hand pannel. 
![](./PcbDesign/footprint.png)
You can also view the *footprint* and also *3d View*.
![](./PcbDesign/3dview.png)
 You can also check if you circuit has faults by using *Electrical rule checker in Inspect*.
 After that you can open the *Pcb Board editor* and then import the components from your schematics.
 ![](./PcbDesign/boardeditor.png)
 You can do that by opening the update PCB board and it will import all your components here.
 ![](./PcbDesign/updatePcb.png)
 ![](./PcbDesign/Imported.png)
 Select the *F.cu layer * given on the right side, which stand for front copper layer.
 You have to route the nets(the thin line connecting the components) by using the *Route tracks* on the right side or by using the shortcut key X.
In the *edge cut* layer make a box either uing the line tool or the *rectangle tool*. This box will be used for cutting out the board. 
By right clicking on the box and properties you can also change the line width. 
 ![](./PcbDesign/pcbboard.png)
 

 # Preparing the board for milling
 Documenting the process in words would rather confuse the readers so I am attching a video by Sibu and Fran which is much more easy to follow.
 ![Preparing the Board for milling](./PcbDesign/PreparingTheBoard.mp4)