# Fab Zero Documentation

Instructed by Fran and Sibu

## Navigation
1. Daily [log](./log.md)

2. Introduction to Fab Academy

    - Introduuction to the [Syllabus](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/summary.md).
    - Introduction to the [Fab Zero] (<https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/basic/intro.md>)
    - Assignment: Brain Strom on a project
3.  Installing **WSL**  (Windows Sub System for Linux) [link](/installingWSL.md)

4. Documenting [uisng Git](./UsingGit.md)
    - how to write in [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#images)
    - How to make a repository
    - How to clone a repo using HTML and SSH key
5. Project management using git
    
6. [Image processing](./ImagesProcessing.md)
    - Taking Screen shots
    - Using Flameshot
    - Using Imagemagik

7. [InkScape](./UsingInkScape.md)