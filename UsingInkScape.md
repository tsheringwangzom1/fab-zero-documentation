# Changing the Raster inages into Vectro images.
**Raster** images are made up of pixels and **Vector** images are made up of lines.
![](./images/vector-raster.jpg)

There are many tools which can be used for changing the vector images into vector images like CoralDraw, Photoshop, illustrater etc. But we are going to use InkScape which is an Open source software. Learn more about [InkScape](https://inkscape.org/)
  
  *In fablab it is a very essential skill that one needs to learn to use machines such as laser cutter and Vinyl cutter.* 
 ### Steps for changing Raster to vector using InkScape
  1. Open InkScape and import the image you want to change.
  ![](./workingwithimages/importingimages.png)
  2. Go to *paths* and
  3. then to *Trace Bitmap*. A menue will pop-up at the side.
  4. For black&white image do a *single scane* and for color images do *multiple scans*
  5. Change the brightness threshold to your liking(changing this threshold makes the links thickers)
  * Tip:for the vinyl cutter the lines shoild not be be thick*
  6. Update the image , the preview will be shown to you.
  7. Then press the apply button once. Pressing apply button creates more copies. 
![](./images/inkscape2.png)
  8. On the orginal Picture, the vector image will be created, just drag somewhere else. 
![](./images/inkscape1.png)

InkScape can also be used for changing the size of the images. 
 Go to files and then to the document properties.On the pages there are many document sizes  that you want.
 ![](./images/resize.png)
 * Tip: always keep a margin around the image (5 on all sides) so there wont be any problem while cutting *

![](./images/vector.svg)

## Changing the Vector(SVG file )images into Raster.
1. Go to the files and click on Export png images
2. You can select what you want to export: page, drawing, selection or custom.
3. you can modify the size of the image and the pixles in *dpi*.

![](./images/vectortoraster.png)

*Tips: For most of the machines 600 dpi is standard but if you are making a small pcb or sth which needs precise measurement use 1200dpi* 