# Multilayer Vectorization and Halftoning

## Multilayer Vectorization

This is mostly useful in making stickers in vinyl cutter. For this we will be using [InkScape](UsingInkScape.md)
Firstly, import the image you want to change into a sticker. In our case, we will be making our SFL logo sticker.

- Resize the images to the your desirable size by using the tool above the image  and also lock your image.
![](./workingwithimages/resize.png)
- Now, we have to vectorize the images into different colors, for this we go to path->tracebit map->select multiple scans-select colors.
- Select the number of colors depending on the number of vinys colors we have.
.
![](./workingwithimages/ColorScans.png)

- Change the background of the image from *document properties* because if you have a white in the image, it will consider that it is a background and not part of the image you want to edit. After that go to filters->colors->colorize.
- Change the color of the images to darker shade so the white appears to be grey.
![](./workingwithimages/colorize.png)
- Now if you ungroup the vector image, you will see that the images have been seperated by colors. If there are some distrotions in the image increase the *speckles* to 25.
![](./workingwithimages/withoutstack.png)
This is only done for some images that you want to have it stacked by using the stack option.
![](./workingwithimages/stack.png)

- Going Back, we undo the *stack* and apply it to the image. Then we delete the raster image and start working on the Vector image. 
- Open *layer* and add layers as many as your stickers need in according to the colors. 
- Ungroup the vector image and then start moving the layered images to differnt layers. Name the layers by color for convenience.

![](./workingwithimages/layer.png)

For alignment, make a new layer with different color, make rectangles maybe. 

![](./workingwithimages/align.png)
Now just safe the image in the .svg format and it is ready for printing. 
Here is a sticker I made with super fablab logo.
![](./workingwithimages/inkscapeLayers.png)


# Halftoning

For halftoning we are using the GIMP app.
This techniques works good for a picture with white background or black background.
- Open the image you want to Halftone. 
- Adjust the contrast of the image by going to *colors->brightness and contrasts"
- play arouund and increase the contrast of the image.
- To apply halftoning, go to  *filter-> distorts->Newsprint*
- Here, select the *color Model* to black and white. Select the pattern to circles or dots or lines, etc. 
You can change the period and angle to get the image you want. 
For laser or vinyl cutter or shopbot, if you have more line or circles it will take more time and the machine will have to do more work. 
![](./workingwithimages/halftoning.png)

If you zoom in the pixles are displayed in grey and white, to remove this we use the *anti-alias* option and make it to 0/1.
![](./workingwithimages/alias.png)
 We do this for vectorizing the image before using it with the machines.  
![](./workingwithimages/gimpImages.png)

Here is video link with better instruction[link](https://www.youtube.com/watch?v=gjD04ZAQDng&list=PLKDpiLmgp6Esmd-bqNYZtjosZFaAtdgNE&index=10&t=171s)