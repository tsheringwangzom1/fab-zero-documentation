## Using FlameShot
For taking screen shots, this app is very useful and versitle.


Create a gitignore file "Temp" and  this folder can be used for the files that you dont want to be pushed into your repo, and this is used here. 
 To do this : 
 1. Create a file called temp in your directory. You can use *mkdir temp*
 2. They type **nano .gitignore**
 3. Write any file  you want to ignore while pushing into your repo. In our case *temp/*.
 4. Save and exit.
 ![](./images/gitignore.png)

![](./images/tempFile.png)
 The above screenshot been taken using flameshots. 
## Using imagemagik

Download ImageMagic from the terminal by by using the given code:
 **sudo apt install imagemagick** in Linux/WSL or **brew install imagemagick** in macOS

Resizing the images :
    - Drop the images in the temp file
    - in the terminal, type *ll* (for knowing the size of the images)
    *ll -h*(this command changes the the size into human readable format)
![](./images/ll.png)

- To make a copy of an image with smaller size use the code below.

    ```
    convert bigimage.jpg -resize 640x640 smallimage.jpg
    ```

    Replace the bigimage.jpg with the name of your image. 640x640 is the size of the image and you can also change it to your preference. bigimage will be replaced by the smallimage.
- If you dont want to make copies but replace the bigimage then use the code below but before that if you don't want to lose the image then make a copy of the image beforeexecuting the code below. can also be

```
mogrify -resize 640x640 bigimage.jpg
```

To modify all the images in a folder with the same extention use:

```
mogrify -resize 640x640 *.jpg
```

 Imagemagic allows you to compose images in a horizontal strip, which is very useful for documenting processes.

 ```
convert image1.jpg image2.jpg -geometry x400 +append stripimage.jpg
```

![](./images/stripimage.png)

 When you try to use the above code for the *png* image , the above codes convert the image into a black image.
If you want to convert *png* image to *jpg* image then use the folloing code:

```
convert bigimage.png .geometry x400 +append strip.jpg
```

For better information visit [link](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/design/cad2d.md)
