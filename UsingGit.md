
# Creating GIT repository and clonining
  ## 1. Creating a project in git account.
  My repository name : FAB ZERO documentry

 ## 2. Cloning the repository using HTTPS connection with your WSL.
  
  Because we are using the WSL in  linux system we have to find the location of the file where the git repo is being saved.
  `\\wsl.localhost\Ubuntu-20.04\home\wangzom`

Now in the Ubunto, we navigate into the folder and the clone the repository.
Followings steps shown in this [Link](https://docs.github.com/en/repositories/creating-and-managing-repositories/cloning-a-repository) was followed.
Enter the following code 
``` bash
 git clone https://github.com/YOUR-USERNAME/YOUR-REPOSITORY
```
## Cloning the Git repository using the SSH key using WSL (in my case Ubunto).
 Firsly we created a folder called *ssh* in the linux directory (`\\wsl.localhost\Ubuntu-20.04\home\your PC Name`) in ur personal folder.
 For that in your linux terminal for my case in Ubuntu, write:

 ```bash
 mkdir ssh
 ```
 then 
 ```bash
 cd ssh
 ```
 This will direct you into the ssh folder.
Paste  the text below and subsitute your Git email address.
  ```bash
  ssh-keygen -t ed25519 -C "your_email@example.com"

  > Enter a file in which to save the key (/c/Users/you/.ssh/id_algorithm):[Press enter]
>  Enter passphrase (empty for no passphrase): [Type a passphrase]

> Enter same passphrase again: [Type passphrase again]

  ```
### Adding the SSH key to the SSH-agent 

Now check if the SSH has be generated  by listing the key
``` bash
ls
```
 Next type the code below to make sure your SSH agent is running.
 ```bash
 $ eval "$(ssh-agent -s)"
> Agent pid 59566
```
Add your SSH private key to the ssh-agent. If you created your key with a different name, or if you are adding an existing key that has a different name, replace id_ed25519 in the command with the name of your private key file.
```bash
$ ssh-add ~/.ssh/id_ed25519
 >Enter passphrase for /home/wangzom/.ssh/id_ed25519:
 ```
 After creating the ssh keypair, and adding it to the ssh-agent, you have to upload the public key to github/gitlab. For copying the contents of the public key to the clipboard you can just open it in any text editor, select all of it contents and copy it. I will use a command line tool called xclip. If you don't have it already, you can install it by typing:
 ```bash
 sudo apt-get install xclip

 xclip -sel clip < ~/.ssh/id_ed25519.pub
 ```
 if you get an error saying
 `Error: Can't open display: (null)`

 type 
 ```
 cat ~/.ssh/id_ed25519.pub
 ```

 Then you just have to paste the key in the SSH keys section of gitlab.
 ![](./images/ssh.png)
Name that key in github/gitlab as the computer you are using. If you loose that computer or you feel that the key is compromised you will know what key to delete.

## Cloning the repository  using a SSH connection
 ```
 cd 
 git clone paste-the-address-here.git
 ```
 ![](./images/key.png)



## Git common codes
```
git status
git pull
git push
git add .
git commit -m "message"
 git push
 ```


  For better reference please visit: [Link](https://gitlab.fabcloud.org/fabzero/fabzero/-/blob/master/program/basic/git.md) and [Generating a new SSH key and adding it to the ssh-agent](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

  